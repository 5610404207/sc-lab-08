package item1;

public interface Measurable {
	
	public double getMeasure();
		
	}
