package item1;

public class Country implements Measurable{
	String name;
	double area;
	
	public Country (String name,double area){
		this.name = name;
		this.area = area;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return area;
	}
}
