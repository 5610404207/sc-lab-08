package item1;

public class Data {
	
	
	public double average (Measurable[] object){
		double sum = 0;
		for (Measurable measurable : object) {
			sum+= measurable.getMeasure();
		}
		if (object.length>0){
			return sum/object.length;
		}
		else{
			return 0;
		}
	}
	
	public double min (Measurable[] object){
		Measurable min = object[0];
		
		for (int i = 1; i < object.length; i++) {
			if (min.getMeasure() > object[i].getMeasure()){
				min = object[i];
			}
		}
		
		return min.getMeasure();
		
	}
	
}
