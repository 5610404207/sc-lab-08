package item1;

public class TestMeasure {

	public static void main(String[] args) {
		Measurable [] person = new Measurable[2];
		person [0] = new Person("kittipat",180.00);
		person [1] = new Person("jirapinya",156.00);
		
		Measurable [] balance = new Measurable[2];
		balance [0] = new BankAccount(500.00);
		balance [1] = new BankAccount(200.00);
		
		Measurable [] area = new Measurable[2];
		area [0] = new Country("Disney",200000.00);
		area [1] = new Country("Hell",100000);
		
		Data data = new Data();
		double averageData = data.average(person);
		System.out.println(averageData);
		
		System.out.println(data.min(person));
		System.out.println(data.min(balance));
		System.out.println(data.min(area));
		
	}
}
