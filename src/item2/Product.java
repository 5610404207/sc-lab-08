package item2;

public class Product implements Taxable {
	String name ;
	double price;
	
	public Product (String name , double price){
		this.name = name ;
		this.price = price;
	}

	@Override
	public double getTax() {
		double tax = 0;
		tax = price *0.07;
		return tax;
	}
	

}
