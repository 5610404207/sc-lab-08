package item2;

public class Company implements Taxable {
	String name ;
	double income;
	double expenses;

	
	public Company (String name , double income,  double expenses){
		this.name = name ;
		this.income = income;
		this.expenses = expenses;
	}
	
	

	@Override
	public double getTax() {
		double tax = 0;
		double profit = income-expenses;
		tax = profit*0.3;
		return tax;
	}
	
}
