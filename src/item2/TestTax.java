package item2;


public class TestTax {
	public static void main(String[] args) {
		Taxable[] salary = new Taxable[2];
		salary [0] = new Persons("kittipat",180.00);
		salary [1] = new Persons("jirapinya",156.00);
		
		Taxable [] balance = new Taxable[2];
		balance [0] = new Company("kittipat",500.00,180.00);
		balance [1] = new Company("jirapinya",200.00,156.00);
		
		Taxable [] area = new Taxable[2];
		area [0] = new Product("Disney",200000.00);
		area [1] = new Product("Hell",100000);
		
		Taxable [] all = new Taxable[3];
		all [0] = new Persons("kittipat",180.00);
		all [1] = new Company("kittipat",500.00,180.00);
		all [2] = new Product("Disney",200000.00);
		
		TaxCalculator tc = new TaxCalculator();
		System.out.println(tc.sum(salary));
		System.out.println(tc.sum(balance));
		System.out.println(tc.sum(area));
		System.out.println(tc.sum(all));

		
	}
}
