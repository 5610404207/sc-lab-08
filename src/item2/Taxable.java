package item2;

public interface Taxable {

	public double getTax();
}
