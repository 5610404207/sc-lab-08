package item2;

public class Persons implements Taxable {
	String name ;
	double salary;
	
	public Persons (String name , double salary){
		this.name = name ;
		this.salary = salary;
	}

	@Override
	public double getTax() {
		double tax = 0;
		if (salary > 300000){
			tax = (salary-300000)*0.1+15000;
		}
		else{
			tax = salary*0.05;
		}
		
		return tax;
	}
	
	
}
